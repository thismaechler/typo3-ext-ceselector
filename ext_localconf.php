<?php
defined('TYPO3_MODE') || die();

(function () {

	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin (
		'MMC.Ceselector',
		'pi1',
		['Selector' => 'render'],
		// non-cacheable actions
		['Selector' => 'render']
	);

})();
